import React from 'react';

const Favorites = ({ favoritesItems, removeFromFavorites }) => {
  return (
    <div>
      <h1 className='head-title'>Favorites</h1>
      <ul className='product-list'>
        {favoritesItems.map(item => (
          <li key={item.article} className={`product-card`}>
            <img className="product-photo" src={item.imageUrl} alt={item.name} />
            <div className='card-text'>
              <h3>{item.name}</h3>
              <p>{`Price: ${item.price}`}</p>
              <p>{`Article: ${item.article}`}</p>
              <p>{`Color: ${item.color}`}</p>
            </div>
            <button className='button-add-to-cart' onClick={() => removeFromFavorites(item)}>Delete</button>
          </li>
        ))}
      </ul>
    </div>
  );
};

export default Favorites;