import React, { useState } from 'react';
import Modal from '../Components/Modal';
import '../Components/Modal.scss';


const Cart = ({ cartItems, removeFromCart }) => {
  const [selectedItem, setSelectedItem] = useState(null);

  const handleRemoveFromCart = (item) => {
    setSelectedItem(item);
  };

  const handleCancelRemove = () => {
    setSelectedItem(null);
  };

  const handleConfirmRemove = () => {
    if (selectedItem) {
      removeFromCart(selectedItem);
      setSelectedItem(null);
    }
  };

  return (
    <div>
      <h1 className='head-title'>Cart</h1>
      <ul className='product-list'>
        {cartItems.map((item) => (
          <li key={item.article} className={`product-card`}>
            <img className="product-photo" src={item.imageUrl} alt={item.name} />
            <div className='card-text'>
              <h3>{item.name}</h3>
              <p>{`Price: ${item.price}`}</p>
              <p>{`Article: ${item.article}`}</p>
              <p>{`Color: ${item.color}`}</p>
            </div>
            <span className="close-btn close-icon" onClick={() => handleRemoveFromCart(item)}>&#x2715;</span>
          </li>
        ))}
      </ul>
      <Modal
        isOpen={!!selectedItem}
        onClose={handleCancelRemove}
        onConfirm={handleConfirmRemove}
        message="Are you sure you want to remove this product from the cart?"
      />
    </div>
  );
};

export default Cart;