import React from 'react';
import PropTypes from 'prop-types';
import './Modal.scss';

const Modal = ({ isOpen, onClose, onConfirm, message }) => {
  const openModal = { display: isOpen ? 'block' : 'none' };
  const overlayAdd = { display: isOpen ? 'block' : 'none' };
  return (
    <>
      {isOpen && (
        <div style={overlayAdd} className="addOverlay" onClick={onClose}>
          <div style={openModal} className="modal">
            <div className="content">
              {onClose && (<span className="close-btn" onClick={onClose}>&#x2715;</span>)}
              <p>{message}</p>
              <div className='btn-container'><button className='button-add-to-cart' onClick={onConfirm}>Yes</button>
                <button className='button-add-to-cart' onClick={onClose}>No</button></div>
            </div>
          </div></div>
      )}
    </>
  );
};
Modal.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  onClose: PropTypes.func.isRequired,
  onConfirm: PropTypes.func.isRequired,
};
export default Modal;
