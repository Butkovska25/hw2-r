import React, { useState, useEffect } from 'react';
import axios from 'axios';
import PropTypes from 'prop-types';
import ProductCard from './ProductCard';

const ProductList = ({ addToCart, addToFavorites, removeFromFavorites }) => {
  const [products, setProducts] = useState([]);

  useEffect(() => {
    axios.get('/products.json')
      .then(response => setProducts(response.data))
      .catch(error => console.error('Error - fetch products', error));
  }, []);

  return (
    <div>
      <h1 className='head-title'>The list of sweets</h1>
      <ul className='product-list'>
        {products.map(product => (
          <ProductCard
            key={product.article}
            product={product}
            addToCart={addToCart}
            addToFavorites={addToFavorites}
            removeFromFavorites={removeFromFavorites}
          />
        ))}
      </ul>
    </div>
  );
};
ProductList.propTypes = {
  addToCart: PropTypes.func.isRequired,
  addToFavorites: PropTypes.func.isRequired,
};

export default ProductList;
