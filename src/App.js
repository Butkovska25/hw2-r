import React, { useState, useEffect } from 'react';
import { BrowserRouter as Router, Route, Routes, Link } from 'react-router-dom';
import ProductList from './Components/ProductList';
import Cart from './Pages/Cart';
import Favorites from './Pages/Favorites';
import Header from './Components/Header';
import './Components/Cards.scss'

const App = () => {
  const [cartItems, setCartItems] = useState(() => {
    const storedCartItems = localStorage.getItem('cartItems');
    return storedCartItems ? JSON.parse(storedCartItems) : [];
  });
  const addToCart = (product) => {
    setCartItems([...cartItems, product]);
  };
  const removeFromCart = (product) => {
    setCartItems(cartItems.filter((item) => item.article !== product.article));
  };
  const [favoritesItems, setFavoritesItems] = useState(() => {
    const storedFavoritesItems = localStorage.getItem('favoritesItems');
    return storedFavoritesItems ? JSON.parse(storedFavoritesItems) : [];
  });
  const addToFavorites = (product) => {
    setFavoritesItems([...favoritesItems, product]);
  };
  const removeFromFavorites = (product) => {
    setFavoritesItems(favoritesItems.filter(item => item.article !== product.article));
  };
  useEffect(() => {
    localStorage.setItem('cartItems', JSON.stringify(cartItems));
  }, [cartItems]);
  useEffect(() => {
    localStorage.setItem('favoritesItems', JSON.stringify(favoritesItems));
  }, [favoritesItems])

  return (
    <Router>
      <div>
        <nav className='nav-container'>
          <ul className='nav-options'>
            <li className='nav-items'><Link to="/">Main Page</Link></li>
            <li className='nav-items'><Link to="/cart">Cart</Link></li>
            <li className='nav-items'><Link to="/favorites">Favorites</Link></li>
          </ul>
        </nav>
        <Header />
        <Routes>
          <Route
            path="/"
            element={<ProductList addToCart={addToCart} addToFavorites={addToFavorites} removeFromFavorites={removeFromFavorites} />}
          />
          <Route
            path="/cart"
            element={<Cart cartItems={cartItems} removeFromCart={removeFromCart} />}
          />
          <Route
            path="/favorites"
            element={<Favorites favoritesItems={favoritesItems} removeFromFavorites={removeFromFavorites} />}
          />
        </Routes>
      </div>
    </Router>
  );
};

export default App;
